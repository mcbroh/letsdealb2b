import React from 'react';
import Jumbotron from 'react-bootstrap/esm/Jumbotron';
import Button from 'react-bootstrap/esm/Button';


interface Props {
    header: string;
    body?: string;
}

export function FlashHeaderAdvert({ header, body }: Props) {

    const bodyText = 'This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.';
    
    return (
        <Jumbotron>
            <h1>{header}</h1>
            <p>{body || bodyText}</p>
            <p>
                <Button variant="primary">Learn more</Button>
            </p>
        </Jumbotron>
    )
}