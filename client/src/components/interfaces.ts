export interface Invoice {
    owner: string;
    bookedBy: string;
    invoicePrice: string;
    totalPrice: string;
    repayment?: number;
    bookingCompanyStatus: string;
}