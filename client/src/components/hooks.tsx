import React, { useReducer, createContext, useContext } from 'react';
import { Invoices } from './constant';
import { Invoice } from './interfaces';

interface Action {
    type: string;
    data: any;
}

interface State {
    processing: boolean;
    offers: any;
    cart: Array<any>;
    user: any;
    invoices: Array<Invoice>,
    showAlert: boolean;
    alertMessage: string | null;
    alertVariant: string;
    showLogin: boolean;
}

const InitialState: State = {
    processing: false,
    invoices: Invoices,
    user: getLocalStorage(),
    offers: localStorage.getItem('Offers') ? JSON.parse(localStorage.getItem('Offers') as any) : [],
    cart: localStorage.getItem('Cart') ? JSON.parse(localStorage.getItem('Cart') as any) : [],
    showAlert: false, 
    alertMessage: null, 
    alertVariant: 'danger',
    showLogin: false
};

const StateContext = createContext<any>(null);
const DispatchContext = createContext<any>(null);

function UiReducer(state: State, action: Action) {
    switch (action.type) {
        case 'setOffers': {
            return { ...state, offers: [...action.data] }
        }
        case 'addToCart': {
            const cart = [...state.cart, { ...action.data, tempKey: Date.now().toString() }]            
            if (!state.user) {
                return { ...state, showLogin: !state.showLogin };
            } else {
                if (state.user.role !== 'subBuyer') {
                    return;
                }
            }
            
            _updateLocalStorage('Cart', cart);

            return { ...state, cart };
        }
        case 'removeFromCart': {
            const cart = state.cart.filter((picked: any) => picked.tempKey !== action.data.tempKey);
            _updateLocalStorage('Cart', cart);

            return { ...state, cart };
        }
        case 'updateUser': {
            if (action.data?.token) {
                _updateLocalStorage('loggedInUser', action.data);
            } else {
                localStorage.removeItem('loggedInUser');
            }
            return { ...state, user: action.data, showLogin: false }
        }
        case 'alert': {
            return { ...state, showAlert: !state.showAlert, ...action.data }
        }
        case 'toggleLoginDialog': {
            return { ...state, showLogin: !state.showLogin }
        }
        case 'clearCart': {
            _updateLocalStorage('Cart', []);
            _updateLocalStorage('loggedInUser', action.data);
            return { ...state, cart: [], user: {...action.data} };
        }
        default: {
            return state;
        }
    }
}

function AppProvider({ children }: any) {
    const [state, dispatch] = useReducer(UiReducer, InitialState);

    return (
        <StateContext.Provider value={state}>
            <DispatchContext.Provider value={dispatch}>
                {children}
            </DispatchContext.Provider>
        </StateContext.Provider>
    );
}

function useAppState() {
    const context = useContext(StateContext);
    if (context === undefined) {
        throw new Error('useAppState must be used within a AppProvider');
    }
    return context;
}

function useAppDispatch() {
    const context = useContext(DispatchContext);
    if (context === undefined) {
        throw new Error('useAppDispatch must be used within a AppProvider');
    }
    return context;
}

function _updateLocalStorage(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
}

function getLocalStorage() {
    const userObj = localStorage.getItem('loggedInUser');
    
    if (userObj) {
        return JSON.parse(userObj);
    }
    return null;
}

export { AppProvider, useAppState, useAppDispatch };
