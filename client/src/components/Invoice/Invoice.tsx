import React, { useState, useEffect } from 'react';

import './Invoice.scss';
import { InvoiceList } from './InvoiceList';
import { useAppState } from '../hooks';
import { LogIn } from '../Admin/LogIn';
import { FlashHeaderAdvert } from '../FlashHeaderAdvert';

function Invoice() {

    const { user, invoices } = useAppState();
    const [isLoggedIn, setLoggedIn] = useState(false);

    useEffect(() => {
        const role = user?.role && (user?.role === 'invoice' || user?.role === 'admin');
        setLoggedIn(role);
    }, [user]);

    return (
        <div className="invoice pb-5 bg-light">
            <div className="container">
                <FlashHeaderAdvert header='Invest in invoices! Make from 5% to 20% return' />
                <h2 className="mb-4">Invoice Trading</h2>
                {isLoggedIn ? <InvoiceList invoices={invoices} /> : <LogIn />}
            </div>
        </div>
    )
}

export default Invoice;