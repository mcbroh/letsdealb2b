import React, { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { InfoCircle } from 'react-bootstrap-icons';

import { Invoice } from '../interfaces';

interface Props {
    invoices: Array<Invoice>
}

export function InvoiceList({ invoices }: Props) {
    
    const [showModal, toggleModal] = useState(false);
    const [selectedInvoice, setSelectedInvoice] = useState<Invoice | null>(null);

    function showInvoiceDetails(invoice: Invoice) {
        setSelectedInvoice(invoice);
        toggleModal(true);
    }

    return (
        <div className='invoice-list py-5 container'>
            <table role="table">
                <thead>
                    <tr role="row">
                        <th role="columnheader">Seller</th>
                        <th role="columnheader">Buyer</th>
                        <th role="columnheader">Invoice Price</th>
                        <th role="columnheader">Total Price</th>
                        <th role="columnheader">Repayment</th>
                        <th role="columnheader"></th>
                    </tr>
                </thead>
                <tbody>
                    {invoices.map((invoice, index) => (
                        <tr key={index} role="row" className="mb-3">
                            <td role="cell">{invoice.owner}</td>
                            <td role="cell">{invoice.bookedBy}</td>
                            <td role="cell">₦ {invoice.invoicePrice}</td>
                            <td role="cell">₦ {invoice.totalPrice}</td>
                            <td role="cell">{invoice.repayment || 12} months</td>
                            <td role="cell">
                                <Button className="mr-2" 
                                    variant="outline-dark" size='sm'
                                    onClick={() => showInvoiceDetails(invoice)}
                                ><InfoCircle /></Button>
                                <Button variant="warning" size='sm'>Buy</Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>

            <Modal show={showModal}>
                <Modal.Header>
                    <h5 className="modal-title">About {selectedInvoice?.bookedBy}</h5>
                </Modal.Header>
                <Modal.Body>
                    <p>{selectedInvoice?.bookingCompanyStatus}</p>
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        variant='secondary'
                        onClick={() => toggleModal(false)}
                    >Close</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}