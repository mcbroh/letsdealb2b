import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { Formik, Form } from 'formik';
import Alert from 'react-bootstrap/Alert'
import * as Yup from 'yup';

import { useAppDispatch } from '../hooks';
import { URLAddress } from '../../helper';
import { useHistory } from 'react-router-dom';
import { AppTextInput } from '../../shared/AppTextInput';

const validationSchema = Yup.object({
    password: Yup.string()
        .min(6, 'Must be minimum 6 characters')
        .required('Password is required'),
    email: Yup.string()
        .email('Invalid email address')
        .required('Email is required'),
});

export function LogIn() {
    const history = useHistory();
    const dispatch = useAppDispatch();
    const [hasError, setHasError] = useState(null);

    return (
        <div className="d-flex justify-content-center flex-column">
            <h1>Sign In</h1>
            {hasError && <Alert className="modal-alert" variant='danger' onClose={() => setHasError(null)} dismissible>{hasError}</Alert>}
            <Formik
                initialValues={{ email: '', password: '' }}
                validationSchema={validationSchema}
                onSubmit={(values, { setSubmitting, resetForm }) => {
                    fetch(URLAddress + 'login', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(values),
                    })
                    .then(response => response.json())
                    .then(data => {
                        setSubmitting(false);
                        if (data.success) {
                           resetForm();
                            dispatch({ type: 'updateUser', data: { ...data.user } });
                            if (data.user.role !== 'subBuyer') {
                                history.replace('/admin');
                            }
                        } else setHasError(data.message);
                    })
                    .catch((error) => {
                        setSubmitting(false);
                        setHasError(error.message);
                    });
                }}
            >
                {({ isSubmitting }) => (
                    <Form>
                        <AppTextInput name="email" label="Email" type="email" />
                        <AppTextInput name="password" label="Password" type="password" />
                        <Button type="submit" className="d-block ml-auto" variant="primary" disabled={isSubmitting}>{isSubmitting ? "Please wait..." : "Submit"}</Button>
                    </Form>
                )}
            </Formik>
        </div>
    )
}