import React, { Fragment, useState, useEffect } from 'react';
import ListGroup from 'react-bootstrap/ListGroup'

import './MemberList.scss'
import { /* useAppState,  */useAppDispatch } from '../../hooks';
import { Cart } from '../../../shared/Cart';
import { URLAddress } from '../../../helper';

export function MemberList() {
    const dispatch = useAppDispatch();
    /*     const { /* user, * cart } = useAppState(); */
    const [loading, setLoading] = useState(false);
    const [members, setMembers] = useState<Array<any>>([]);
    const [products, setProducts] = useState<Array<any>>([]);
    const [selectedDetail, setSelected] = useState<string | null>(null);

    useEffect(() => {
        getProducts();
        getCode();
        // eslint-disable-next-line
    }, []);

    function getProducts() {
        fetch(`${URLAddress}products/list`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(response => {
                return response.json()
            })
            .then(data => {
                if (data.success) {
                    setProducts(data.products);
                }
            })
            .catch((error) => {
                dispatch({ type: 'alert', data: { alertMessage: error.message } })
            });
    }

    function getCode() {
        setLoading(true);
        const user = JSON.parse(localStorage.getItem('loggedInUser') || '{}')
        fetch(`${URLAddress}buyer/members`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user.token,
            },
        })
        .then(response => {
            if (response.status === 401) {
                dispatch({ type: 'updateUser', data: {} });
            }
            return response.json()
        })
        .then(data => {
            setLoading(false);
            if (data.success) {
                setMembers(data.members);
            } else dispatch({
                type: 'alert', data: {
                    alertVariant: 'danger',
                    alertMessage: data.message
                }
            });
        })
        .catch((error) => {
            setLoading(false);
            dispatch({ type: 'alert', data: { alertMessage: error.message } })
        });
    }

    function listClicked(details: any) {
        details = details === selectedDetail ? null : details;
        setSelected(details);
    }

    function shoppingDetails(details: any) {
        const cart: Array<any> = [];
        (details.products || []).forEach((prod: any) => {
            let bookedCopy = prod.booked;
            while (bookedCopy > 0) {
                bookedCopy--;
                cart.push(products.find( ({ id }) => id === prod.productID));
            }
        });
        return (
            <div className="shopping-details p-3">
                <Cart cart={cart} />
            </div>
        )
    }
      

    return (
        <div className="d-flex justify-content-center flex-column mb-5 member-list">
            <h3>Members {loading && ' loading...'}</h3>
            <ListGroup horizontal className="list-container">
                <ListGroup.Item variant="primary">Name</ListGroup.Item>
                <ListGroup.Item variant="primary">Telephone</ListGroup.Item>
                <ListGroup.Item variant="primary">email</ListGroup.Item>
            </ListGroup>
            <div className="scroll-div">
                {members.map((details: any) => (
                    <Fragment>
                        <ListGroup key={details.id} horizontal className="list-container" onClick={() => listClicked(details)}>
                            <ListGroup.Item>{details.fullName}</ListGroup.Item>
                            <ListGroup.Item>{details.telephone}</ListGroup.Item>
                            <ListGroup.Item>{details.email}</ListGroup.Item>
                        </ListGroup>
                        {selectedDetail === details && shoppingDetails(details)}
                    </Fragment>
                ))}
            </div>
        </div>
    )
}