import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import { useHistory } from 'react-router-dom';
import { URLAddress } from '../../helper';

import { useAppDispatch } from '../hooks';

const registerDetails = [
    {
        label: 'Code*',
        type: 'text',
        key: 'token',
        hide: [],
    },
    {
        label: 'Email*',
        type: 'email',
        key: 'email',
        hide: [],
    },
    {
        label: 'Password*',
        type: 'password',
        key: 'password',
        hide: ['invoice', 'buyer'],
    },
    {
        label: 'Full Name*',
        type: 'text',
        key: 'fullName',
        hide: [],
    },
    {
        label: 'Telephone*',
        type: 'tel',
        key: 'telephone',
        hide: [],
    },
    {
        label: 'Company Name*',
        type: 'text',
        key: 'companyName',
        hide: ['seller', 'invoice', 'buyer'],
    },
    {
        label: 'Company Email*',
        type: 'email',
        key: 'companyEmail',
        hide: ['seller', 'invoice', 'buyer'],
    },
    {
        label: 'Company Telephone*',
        type: 'tel',
        key: 'companyTelephone',
        hide: ['seller', 'invoice', 'buyer'],
    }
];

export function Register() {
    const history = useHistory();
    const dispatch = useAppDispatch();
    const [loading, setLoading] = useState(false);
    const [form, setForm] = useState<any>({});

    function doSubmit() {
        setLoading(true);
        fetch(`${URLAddress}new-user`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(form),
        })
            .then(response => {
                if (response.status === 401) {
                    dispatch({ type: 'updateUser', data: {} });
                }
                return response.json()
            })
            .then(data => {
                setLoading(false);
                if (data.success) {
                    dispatch({
                        type: 'alert',
                        data: {
                            alertVariant: 'success',
                            alertMessage: 'User created successfully'
                        }
                    });
                    setForm({});
                    history.replace('/');
                } else dispatch({
                    type: 'alert', data: {
                        alertVariant: 'danger',
                        alertMessage: data.message
                    }
                });
            })
            .catch((error) => {
                setLoading(false);
                dispatch({ type: 'alert', data: { alertMessage: error.message } })
            });
    }

    function getInput(obj: any) {
        return (<div key={obj.key} className="form-group">
            <label htmlFor={obj.key}>{obj.label}</label>
            <input
                className="form-control"
                type={obj.type}
                value={form[obj.key] || ''}
                id={obj.key}
                onChange={(e) => setForm({
                    ...form,
                    [obj.key]: e.target.value
                })} />
        </div>)
    }

    return (
        <div className="d-flex container justify-content-center flex-column mb-5">
            <h3>Register User</h3>
            <form>
                {registerDetails.map(obj => getInput(obj) )}
            </form>
            <Button variant='primary' disabled={loading} onClick={() => doSubmit()}>
                {loading ? 'Please wait...' : 'Submit'}
            </Button>
        </div>
    )
}