import React, { useEffect, useState, Fragment } from 'react';
import { useHistory } from 'react-router-dom';

import './Admin.scss';
import { LogIn } from './LogIn';
import { useAppState } from '../hooks';
import Button from 'react-bootstrap/esm/Button';
import { MemberList } from './Buyer/MemberList';
import Seller from '../Seller/Seller';
import { AppAdmin } from './AppAdmin/AppAdmin';

function Admin() {

    const history = useHistory();
    const { user } = useAppState();
    const [isLoggedIn, setLoggedIn] = useState(false);
    const [elementToView, setElementToView] = useState<string | null>(null);

    useEffect(() => {
        if (!user || user?.role === 'subBuyer') {
            history.replace('/');
        }
    });

    useEffect(() => {
        const role = user?.role;
        setLoggedIn(role);
    }, [user]);

    function getBuyerButtons() {
        return (
            <Fragment>
                <p className="my-3">Member code: <b>{user?.memberId}</b></p>
                <div className="row mb-3">
                    <div className="col-xs-6 col-sm-6 second holder">
                        <Button block variant="outline-primary" onClick={() => setElementToView('show-members')}>Show Members</Button>
                    </div>
                </div>
            </Fragment>
        )
    }

    function elementToDisplay() {
        switch (elementToView) {
            case 'show-members':
                return <MemberList />;
            default:
                return null;
        }
    }

    return (
        <div className="admin container">
            {isLoggedIn ? (
                <Fragment>
                    <h1 className="my-3">Hello {user?.fullName}</h1>
                    {user?.role === 'buyer' && getBuyerButtons()}
                    {user?.role === 'admin' && <AppAdmin />}
                    {user?.role === 'seller' && <Seller />}
                    {elementToDisplay()}
                </Fragment>
            ) : <LogIn />}
        </div>
    )
}

export default Admin;