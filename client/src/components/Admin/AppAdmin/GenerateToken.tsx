import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';

import '../Admin.scss';
import { useAppState, useAppDispatch } from '../../hooks';
import Button from 'react-bootstrap/esm/Button';
import DropdownButton from 'react-bootstrap/esm/DropdownButton';
import Dropdown from 'react-bootstrap/esm/Dropdown';
import { URLAddress } from '../../../helper';
import { getStringFromVariable } from './helper';


interface Form {
    email: string;
    role: string | null;
    rights: Array<string>;
}

export function GenerateToken() {

    const dispatch = useAppDispatch();
    const { user } = useAppState();
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState<string | null>(null);
    const [form, setForm] = useState<Form>({
        role: null,
        email: '',
        rights: []
    });

    const isValid = () => form.role && form.email;

    function generateCode() {
        if (!isValid) return;
        setLoading(true);
        const user = JSON.parse(localStorage.getItem('loggedInUser') || '{}')
        fetch(`${URLAddress}generateCode`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user?.token,
            },
            body: JSON.stringify(form),
        })
            .then(response => {
                if (response.status === 401) {
                    dispatch({ type: 'updateUser', data: {} });
                }
                return response.json()
            })
            .then(data => {
                setLoading(false);
                if (data.success) {
                    setMessage(data.message);
                    setForm({ email: '', role: null, rights: [] });
                    dispatch({
                        type: 'alert',
                        data: {
                            alertVariant: 'success',
                            alertMessage: 'Code generated'
                        }
                    });
                } else dispatch({
                    type: 'alert', data: {
                        alertVariant: 'danger',
                        alertMessage: data.message
                    }
                });
            })
            .catch((error) => {
                setLoading(false);
                dispatch({ type: 'alert', data: { alertMessage: error.message } })
            });
    }

    function changeRights(e: any) {
        let rights = [...form.rights];
        if (e.target.checked) {
            rights = [e.target.name];
        } else {
            rights = rights.filter(r => r !== e.target.name);
        }

        setForm({ ...form, rights: [...rights] });
    }

    function rolesDropdown() {
        return (
            <DropdownButton variant="secondary" title={form.role || 'Add Role'} className="d-block col-6 col-sm-3 second holder mx-auto mb-2 mt-1">
                {Object.keys(user?.roles || {}).map(role => {
                    const rights = user.rights;
                    const canShow = rights.includes(user.roles[role].canShow) || rights.includes(user.roles[role].override);
                    return canShow ? (
                        <Dropdown.Item
                            key={role}
                            onClick={() => setForm({ ...form, role })}>
                            {user.roles[role].roleName}
                        </Dropdown.Item>
                    ) : null;
                })}
            </DropdownButton>
        )
    }

    function setAdminRole() {
        const rights = user.roles.admin.rights;
        return (
            <Form.Group>
                {rights.map((right: string) => (
                    <Form.Check key={right} checked={form.rights.includes(right)} name={right} type="checkbox" label={getStringFromVariable(right)} onChange={changeRights} />
                ))}
            </Form.Group>
        )
    }

    return (
        <div className="row m-3">
            <h2 className="col-12">Generate Registration token</h2>

            <div className="col-12 col-sm-6 mb-2 mt-1">
                <input
                    className="form-control"
                    type="email"
                    value={form.email}
                    placeholder="email"
                    onChange={(e) => setForm({ ...form, email: e.target.value })} />
            </div>
            {rolesDropdown()}
            <div className="col-6 col-sm-3 second holder mx-auto mb-2 mt-1">
                <Button block variant="outline-primary" disabled={!isValid() || loading} onClick={() => generateCode()}>Send Mail</Button>
            </div>
            {form.role === 'admin' && (
                <div className="col-12 my-2">
                    <h2 className="text-small m-0">Rights</h2>
                    {setAdminRole()}
                </div>
            )}
            <div className="col-12 second holder mx-auto my-3">
                {message && <p>Mail sent successfully: <b><a href={message}>{message}</a></b></p>}
            </div>
        </div>
    )

}
