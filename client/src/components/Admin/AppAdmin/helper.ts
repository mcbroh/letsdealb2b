export function getStringFromVariable(arg: string) {
    const upperCaseWords = arg.match(/([A-Z])/g);
    let text = arg;
    
    if (upperCaseWords) {
        upperCaseWords.forEach(x => {
            text = text.substring(0, text.indexOf(x)) + " " + text.substring(text.indexOf(x)).toLowerCase();
        });
    }
    
    return text.charAt(0).toUpperCase() + text.slice(1);
}