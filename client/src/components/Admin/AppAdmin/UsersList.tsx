import React from 'react';
import Table from 'react-bootstrap/Table';
import BootstrapTable from 'react-bootstrap-table-next';

import { useAppState } from '../../hooks';


export function UsersList() {

    const { user } = useAppState();

    const columns = [{
        dataField: 'fullName',
        text: 'Full Name'
    }, {
        dataField: 'role',
        text: 'Role'
    }, {
        dataField: 'rights',
        text: 'Rights'
    }];

    const expandRow = {
        renderer: (row: any) => (
            <Table striped bordered hover>
                <tbody>
                    {user.allActivities.filter((x: any) => x.creator === row.id).map((activity: any) => (
                        <tr key={activity.id}><td>{`Token creation: token ${activity.id}, role ${activity.role}`}</td></tr>
                    ))}
                </tbody>
            </Table>
        )
    };

    return (
        <div className="row m-3">
            <h2 className="col-12">Admins List</h2>
            <div className="col-12">
                <BootstrapTable
                    keyField='id'
                    data={user.otherUsers}
                    columns={columns}
                    expandRow={expandRow}
                    striped
                    hover
                />
            </div>
        </div>
    )

}
