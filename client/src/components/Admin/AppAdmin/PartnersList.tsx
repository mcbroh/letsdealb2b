import React, { useState } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import Button from 'react-bootstrap/Button';
import { URLAddress } from '../../../helper';
import { useAppDispatch, useAppState } from '../../hooks';


export function PartnersList({ partner }: { partner: 'allSeller' | 'allBuyer' }) {

    const { user } = useAppState();

    const columns = [{
        dataField: 'companyName',
        text: 'Name'
    }, {
        dataField: 'companyEmail',
        text: 'Email'
    }, {
        dataField: 'companyTelephone',
        text: 'Telephone'
    }];

    const expandRow = {
        renderer: (row: any) => <UserOptions selectedUser={(row)} />
    };

    return (
        <div className="row m-3">
            <h2 className="col-12">{partner === 'allSeller' ? 'Sellers' : 'Buyers'} List</h2>
            <div className="col-12">
                <BootstrapTable
                    keyField='id'
                    data={user[partner]}
                    columns={columns}
                    expandRow={expandRow}
                    striped
                    hover
                />
            </div>
        </div>
    )

}

function UserOptions({ selectedUser }: { selectedUser: any }) {
     const dispatch = useAppDispatch();
    const { user } = useAppState();

    const [userData, setUserData] = useState(selectedUser);
    const [loading, setLoading] = useState(false);

    function updateUser(active: boolean) {
        setUserData({ ...userData, active });
    }
    function onSubmit() {
        setLoading(true);
        fetch(`${URLAddress}update-user`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user?.token,
            },
            body: JSON.stringify(userData),
        })
        .then(response => {
            if (response.status === 401) {
                dispatch({ type: 'updateUser', data: {} });
            }
            return response.json()
        })
        .then(data => {
            setLoading(false);
            if (data.success) {
                dispatch({
                    type: 'alert',
                    data: {
                        alertVariant: 'success',
                        alertMessage: 'User created successfully'
                    }
                });
            } else dispatch({
                type: 'alert', data: {
                    alertVariant: 'danger',
                    alertMessage: data.message
                }
            });
        })
        .catch((error) => {
            setLoading(false);
            dispatch({ type: 'alert', data: { alertMessage: error.message } })
        });
    }
    return (
        <div className="col-12">
            <div className="form-check">
                <input className="form-check-input" type="radio" name="exampleRadios" onClick={() => updateUser(false)} checked={!userData.active} />
                <label className="form-check-label" onClick={() => updateUser(false)}>
                    Active
                </label>
            </div>
            <div className="form-check">
                <input className="form-check-input" type="radio" name="exampleRadios" onClick={() => updateUser(true)} checked={userData.active} />
                <label className="form-check-label" onClick={() => updateUser(true)}>
                    Deactivate
                </label>
            </div>
            <Button onClick={onSubmit} disabled={loading}>Update</Button>
        </div>
    )
}
