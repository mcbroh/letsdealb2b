import React, { useState } from 'react';

import Button from 'react-bootstrap/esm/Button';
import { useAppState } from '../../hooks';
import { GenerateToken } from './GenerateToken';
import { PartnersList } from './PartnersList';
import { UsersList } from './UsersList';

export function AppAdmin() {

    const { user } = useAppState();
    const [activeView, setActiveView] = useState<string>('users');

    const buttons = [
        {
            name: user.rights.includes('superAdmin') ? 'Admins Users' : 'My activities',
            key: 'users'
        },
        {
            name: 'List Sellers',
            key: 'sellers'
        },
        {
            name: 'List Buyers',
            key: 'buyers'
        },
        {
            name: 'Generate Token',
            key: 'token'
        },
    ]

    function getTable() {
        switch (activeView) {
            case 'users':
                return <UsersList />;
            case 'token':
                return <GenerateToken />;
            case 'sellers':
                return <PartnersList partner='allSeller' />;
            case 'buyers':
                return <PartnersList partner='allBuyer' />;
        }
    }

    return (
        <div className="row mb-3">
            {buttons.map(button => (
                <div className="col-12 col-sm-6 mb-1" key={button.key}>
                    <Button
                        block
                        variant={`${activeView === button.key ? 'primary' : 'outline-primary'}`}
                        className="float-right"
                        onClick={() => setActiveView(button.key)}>{button.name}</Button>
                </div>
            ))}
            <div className="container">
                {getTable()}
            </div>
        </div>
    )

}
