import React from 'react';
import { useAppState, useAppDispatch } from './hooks';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';

const InitialAlertState = {
    alertMessage: '', 
    alertVariant: 'danger'
};

export function AppAlert() {
    const dispatch = useAppDispatch();
    const { showAlert, alertMessage, alertVariant } = useAppState();

    function formatMessage(message: string | object) {
        let stringValue = JSON.stringify(message);
        stringValue = stringValue.replace('{','');
        stringValue = stringValue.replace('}','');
        stringValue = stringValue.replace(`",`,`", `);

        return stringValue;
    }

    return showAlert ? (
        <div className="container app-alert">
            <Alert
                variant={alertVariant}>
                <Alert.Heading>{alertVariant === 'danger' ? 'Error!' : 'Success'}</Alert.Heading>
                <p>{formatMessage(alertMessage)}</p>
                <hr />
                <div className="d-flex justify-content-end">
                  <Button 
                    variant={'outline-' + alertVariant}
                    onClick={() => dispatch({ type: 'alert', data: InitialAlertState })}>
                    Close
                  </Button>
                </div>
            </Alert>
        </div>
    ) : (
        null
    );
}