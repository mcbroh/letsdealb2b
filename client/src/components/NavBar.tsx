import './NavBar.scss';

import React, { Fragment, ReactElement } from 'react';
import { Navbar, Nav, NavDropdown, Badge, Button, NavLink } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import { BagFill } from 'react-bootstrap-icons';

import { useAppState, useAppDispatch } from './hooks';
import { LogIn } from './Admin/LogIn';
import { useHistory } from 'react-router-dom';

export function NavBar(): ReactElement {
    const dispatch = useAppDispatch();
    const history = useHistory();
    const { cart, user, showLogin } = useAppState();

    function toggleLogin() {
        dispatch({ type: 'toggleLoginDialog' });
    }

    const cartElement = () => (
        user && user.role === 'subBuyer' ? (
            <Navbar.Brand className="app-badge-container" onClick={() => history.replace('/checkout')}>
                <BagFill className="bag" />
                <Badge className={`app-badge ${cart.length > 9 ? 'move-right' : ''}`} variant="light">
                    {cart.length}
                </Badge>
            </Navbar.Brand>
        ) : null
    );

    const canShowShopLink = () => !user || Boolean(user?.role === 'subBuyer') ;

    const navLinksElement = () => (
        <Fragment>
            {canShowShopLink() && <NavDropdown.Item as={NavLink} to="/" href="/">Shop</NavDropdown.Item>}
            <NavDropdown.Item as={NavLink}>Contact Us</NavDropdown.Item>
            {user ? (
                <NavDropdown.Item as={NavLink}
                    onClick={() => {
                        dispatch({ type: 'updateUser', data: null });
                    }}>Logout</NavDropdown.Item>
            ) : (
                <Fragment>
                    <NavDropdown.Item as={NavLink} onClick={toggleLogin}>Login</NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/register" href="/register">Register</NavDropdown.Item>
                </Fragment>
            )}
        </Fragment>
    )

    return (
        <div className="container app-navBar">
            <div className="d-lg-none">
                <Navbar collapseOnSelect expand="sm" variant="light">
                    {cartElement()}
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="ml-auto">
                            {navLinksElement()}
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <section className="my-3">
                    <h1>Lets Deal B2B</h1>
                    <p>Getting you the best deals!</p>
                    <Button variant="dark" size="sm" href='tel:7712365' block>
                        Call Us 7712365
                    </Button>
                </section>
            </div>
            <div className="d-none d-lg-block">
                <Navbar expand="sm" variant="light">
                    <Navbar.Brand href="/">
                        <h1>Lets Deal B2B</h1>
                        <p>Getting you the best deals!</p>
                    </Navbar.Brand>
                    {cartElement()}
                </Navbar>
                <Navbar bg="dark" variant="dark">
                    <Nav className="mr-auto">
                        {navLinksElement()}
                    </Nav>
                    <Button variant="dark" size="sm" href='tel:7712365'>
                        Call Us 7712365
                    </Button>
                </Navbar>
            </div>
            <LoginDialog show={showLogin} onHide={toggleLogin} />
        </div>
    )
}

interface Props {
    show: boolean;
    onHide: () => void;
}

function LoginDialog({ show, onHide }: Props) {
    return (
        <Modal
            show={show}
            size="lg"
            onHide={onHide}
            centered
            backdrop="static"
        >
            <Modal.Header closeButton />
            <Modal.Body>
                <LogIn />
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={onHide}>Close</Button>
            </Modal.Footer>
        </Modal>
    );
}