import React, { useState, Fragment, useEffect } from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import DatePicker from 'react-datepicker';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';

import './Seller.scss';
import { useAppDispatch, useAppState } from '../hooks';
import { LogIn } from '../Admin/LogIn';
import { Offer, Single as SingleProduct } from '../Product/Single';
import { FlashHeaderAdvert } from '../FlashHeaderAdvert';
import { URLAddress } from '../../helper';

dayjs.extend(utc);
dayjs.extend(timezone);

const views = [
    {
        btnName: 'Products List',
        ctx: 'productsList'
    },
    {
        btnName: 'New Offer',
        ctx: 'newOffer'
    },
];

const InitialData = {
    productName: '',
    conditions: '',
    details: '',
    images: ['https://placehold.it/350X350'],
    marketPrice: '00.00',
    offerPrice: '00.00',
    productDescription: '',
    qualifyingPieces: 0,
    startDate: new Date(),
    endDate: dayjs().tz('Africa/Lagos').add(2, 'week').toDate()
};

function Seller() {
    const dispatch = useAppDispatch();
    const { user } = useAppState();
    const [products, setProducts] = useState<Array<any>>([]);
    const [loading, setLoading] = useState(false);
    const [isLoggedIn, setLoggedIn] = useState(false);
    const [activePage, setActivePage] = useState<string>('productsList');
    const [offer, setOffer] = useState<Offer>(InitialData);

    useEffect(() => {
        const role = user?.role && (user?.role === 'seller' || user?.role === 'admin');
        setLoggedIn(role);
        // eslint-disable-next-line
    }, [user]);

    useEffect(() => {
        getMyProduct();
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        setOffer({ ...offer, endDate: dayjs(offer.startDate).tz('Africa/Lagos').add(2, 'week').toDate() });
        // eslint-disable-next-line
    }, [offer.startDate]);

    function getMyProduct() {
        fetch(`${URLAddress}products/my-products`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user?.token}`
            },
        })
            .then(response => {
                if (response.status === 401) {
                    dispatch({ type: 'updateUser', data: {} });
                }
                return response.json()
            })
            .then(data => {
                if (data.success) {
                    setProducts(data.products)
                }
            })
            .catch((error) => {
            });
    }

    const newOffer = () => (
        <Fragment>
            <h2>Create Offer </h2>
            <SingleProduct
                interactive={true}
                offer={offer}
                onChange={setOffer} />
            <div className="row mb-3">
                <div className="col-12 col-sm-6 my-1">
                    <p className="m-0">Offer Start Date</p>
                    <DatePicker selected={offer.startDate} onChange={date => setOffer({ ...offer, startDate: date as Date })} />
                </div>
                <div className="col-12 col-sm-6 my-1">
                    <p className="m-0">Offer End Date</p>
                    <DatePicker disabled={true} selected={offer.endDate as Date} onChange={date => setOffer({ ...offer, startDate: date as Date })} />
                </div>
            </div>
            <Button
                variant='primary mb-5'
                className="float-right"
                disabled={loading}
                onClick={onSubmit}>Add Product</Button>
        </Fragment>
    );

    function onSubmit() {
        setLoading(true);
        const user = JSON.parse(localStorage.getItem('loggedInUser') || '{}')
        fetch(`${URLAddress}products/new `, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + user?.token,
            },
            body: JSON.stringify({ ...offer }),
        })
            .then(response => {
                if (response.status === 401) {
                    dispatch({ type: 'updateUser', data: {} });
                }
                return response.json()
            })
            .then(data => {
                setLoading(false);
                if (data.success) {
                    setOffer(InitialData);
                    setProducts(data.products)
                } else dispatch({
                    type: 'alert', data: {
                        alertVariant: 'danger',
                        alertMessage: data.message
                    }
                });
            })
            .catch((error) => {
                setLoading(false);
                dispatch({ type: 'alert', data: { alertMessage: error.message } })
            });
    }

    function getActivePage() {
        switch (activePage) {
            case 'productsList':
                return <ProductsTable products={products} />;
            case 'newOffer':
                return newOffer();
        }
    }

    return (
        <div className="container pt-2 seller">
            <FlashHeaderAdvert header='Sell your products fast, All payments are immediate' />
            {isLoggedIn ? (
                <div>
                    <div className="row my-3">
                        {views.map(view => (
                            <div key={view.ctx} className="col-xs-12 col-sm-3 my-1">
                                <Button className={`round btn-block mb-1 ${activePage === view.ctx ? 'active' : ''}`} variant="outline-danger" onClick={() => setActivePage(view.ctx)}>{view.btnName}</Button>
                            </div>
                        ))}
                    </div>
                    {getActivePage()}
                </div>
            ) : (
                    <LogIn />
                )}
        </div>
    )
}

export default Seller;

function ProductsTable({ products }: any) {

    return (
        <div>
            <h1>Products on sales</h1>
            <Table striped hover>
                <thead>
                    <tr role="row">
                        <th role="columnheader">Product</th>
                        <th role="columnheader">Offered</th>
                        <th role="columnheader">Left</th>
                        <th role="columnheader">Booked</th>
                    </tr>
                </thead>
                <tbody>
                    {products.map((product: any) => (
                        <tr key={product.id} role="row" className="mb-3">
                            <td role="cell">{product.productName}</td>
                            <td role="cell">{product.qualifyingPieces}</td>
                            <td role="cell">{product.left}</td>
                            <td role="cell">{product.booked}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>

    )
}