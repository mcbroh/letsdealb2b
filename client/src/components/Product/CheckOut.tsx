import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';

import './CheckOut.scss';
import { useAppDispatch, useAppState } from '../hooks';
import { Cart } from '../../shared/Cart';
import { URLAddress } from '../../helper';
import { MyOrder } from '../../shared/MyOrder';

export function CheckOut() {
    const { cart, user } = useAppState();
    const dispatch = useAppDispatch();

    const [loading, setLoading] = useState(false);

    const cartHasProduct = () => cart.length > 0;

    function handleSubmit(event: any) {
        const products: any = {};
        cart.forEach((item: any) => {
            products[item.id] = {
                id: item.id,
                quantity: (products[item.id]?.quantity || 0) + 1
            }
        });

        setLoading(true);
        fetch(URLAddress + 'products/buy', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.token}`
            },
            body: JSON.stringify({ products: Object.values(products) }),
        })
            .then(response => response.json())
            .then(data => {
                setLoading(false);
                if (data.success) {
                    dispatch({ type: 'clearCart', data: data.user });
                }
            })
            .catch((error) => {
                setLoading(false);
            });
    }


    return (
        <div className="container my-5 check-out">
            <h1 className="my-3">My Cart</h1>
            <Cart cart={cart} canDelete={true} />
            <Button
                block
                size="lg"
                disabled={!cartHasProduct() || loading}
                className="d-md-none mt-5"
                variant="warning"
                onClick={handleSubmit}
            >Checkout</Button>
            <Button
                size="lg"
                disabled={!cartHasProduct() || loading}
                className="align-right d-none d-md-block mt-5 ml-auto"
                variant="warning"
                onClick={handleSubmit}
            >Checkout</Button>

            <h1 className="my-5">My Orders</h1>
            <MyOrder />
        </div>
    )

}