import React, { useState, useEffect } from 'react';

import './ProductList.scss';
import { Single } from './Single';
import { useAppDispatch, useAppState } from '../hooks';
import { URLAddress } from '../../helper';
import { useHistory } from 'react-router-dom';

let tried = 0;

export function ProductList() {
    const history = useHistory();
    const dispatch = useAppDispatch();
    const { offers, user } = useAppState();

    const [filteredOffers, setFilteredOffers] = useState<Array<any>>([]);

    useEffect(() => {
        let timer: any = null;
        if (user?.role && user.role !== 'subBuyer') {
            history.replace('/admin')
        } else {
            getOffers();
           timer = setInterval(() => {
                getOffers();
            }, 500000);
        }
        return () => clearInterval(timer);

        // eslint-disable-next-line
    }, []);

    useEffect(() => {
        setFilteredOffers([...offers]);
    }, [offers]);


    function getOffers() {
        fetch(URLAddress + 'products/list')
            .then(response => {
                response.json().then(data => {
                    dispatch({ type: 'setOffers', data: data.products });
                    setFilteredOffers([...data.products]);
                });
            })
            .catch(err => {
                tried < 3 && getOffers();
                tried++;
            });
    }

    function filterProducts(event: any) {
        const value = event.target.value;
        if (value) {
            const searchResult = offers.filter((x: any) => x.productName.toLowerCase().includes(value.toLowerCase()));
            setFilteredOffers([...searchResult]);
        } else {
            setFilteredOffers([...offers]);
        }
    }

    return (
        <div className="product-list pb-5 bg-light">
            <div className="container">
                <div className="row">
                    <div className="col-12 mb-2">
                        <input
                            className="form-control"
                            placeholder="Search product"
                            onChange={filterProducts} />
                    </div>
                    {filteredOffers.map((offer: any, index: any) => (
                        <div key={index} className="col-6 p-1">
                            <Single offer={offer} />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}