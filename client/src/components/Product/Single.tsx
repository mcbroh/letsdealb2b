import './Single.scss';

import React, { ReactElement, useState, Fragment, useRef, useEffect } from 'react';
import { InputGroup, FormControl } from 'react-bootstrap';
import Button from 'react-bootstrap/esm/Button';
import Modal from 'react-bootstrap/esm/Modal';
import { BagPlus } from 'react-bootstrap-icons';
import Overlay from 'react-bootstrap/Overlay';

import { useAppDispatch, useAppState } from '../hooks';
import { _calculateTimeLeft } from '../../helper';

export interface Offer {
    booked?: number;
    conditions: string;
    details: string;
    id?: string;
    images: string[]
    left?: number;
    marketPrice: string;
    offerPrice: string;
    owner?: string;
    productDescription: string;
    productName: string;
    qualifyingPieces: number;
    startDate?: Date;
    endDate?: Date | string;
}

interface Props {
    offer: Offer;
    showDetails?: boolean;
    badgeColor?: string;
    interactive?: boolean;
    onChange?: (change: Offer) => void;
}

export function Single({ offer, showDetails = false, badgeColor = 'grey', interactive = false, onChange }: Props): ReactElement {

    const dispatch = useAppDispatch();
    const { user } = useAppState();
    const canvasRef = React.useRef(null);

    const target = useRef(null);
    const [show, setShow] = useState(false);
    const inputFileRef = useRef<any>(null);
    const [showModal, toggleModal] = useState(false);
    const [selectedOffer, setSelectedOffer] = useState<any>({});
    const [timeLeft, setTimeLeft] = useState(_calculateTimeLeft(offer?.endDate));

    const offerExists = (offer.left || 0) > 0;
    const getAmount = (amount: number) => 'NGN ' + amount + ',000';

    useEffect(() => {
        if (offer?.endDate) {
            const timer = setTimeout(() => {
                setTimeLeft(_calculateTimeLeft(offer?.endDate));
            }, 1000);
            return () => clearTimeout(timer);
        }
    });

    useEffect(() => {
        if (show) {
            setTimeout(() => {
                setShow(false);
            }, 3000);
        }
    }, [show]);

    function getClass(left: number) {
        if (left < 5) return 'danger';
        if (left < 10) return 'warning';
        return 'success'
    }

    function showOfferCondition(offer: any) {
        setSelectedOffer(offer);
        toggleModal(true);
    }

    function changeHappened(change: object) {
        onChange && onChange({ ...offer, ...change })
    }

    const handleChange = (e: any) => {
        if (e.target.files.length) {
            transformFileSize(e.target.files[0])
                .then((base64string: any) => {
                    offer.images[0] = base64string;
                    changeHappened({ images: [...offer.images] })
                });
        }
    };

    function transformFileSize(file: any) {
        return new Promise(resolve => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                const canvas: any = canvasRef.current;
                const img: any = document.createElement('img');
                img.src = reader.result;
                img.onload = () => {
                    const ctx = canvas.getContext('2d');
                    let max_size = 550,
                        width = img.width,
                        height = img.height;
                    if (width > height) {
                        if (width > max_size) {
                            height *= max_size / width;
                            width = max_size;
                        }
                    } else {
                        if (height > max_size) {
                            width *= max_size / height;
                            height = max_size;
                        }
                    }
                    canvas.width = width;
                    canvas.height = height;
                    ctx.drawImage(img, 0, 0, width, height);
                    const base64 = canvas.toDataURL(img.mime_type);
                    resolve(base64);
                };
            };
        });
    }

    function addToCartButton() {        
        return !interactive ? (
            <Button variant="warning"
                ref={target}
                onClick={() => {
                    dispatch({ type: 'addToCart', data: offer });
                    user && setShow(!show);
                }}
                disabled={!offerExists || (user && user.role !== 'subBuyer')} className="btn btn-sm">
                <BagPlus className="add-to-cart" />
            </Button>
        ) : null;
    }

    function cardFooter() {
        return (
            <Fragment>
                <canvas className="d-none" id='canvasId' ref={canvasRef} />
                {!interactive && user && user.role !== 'subBuyer' && <p className="d-flex m-0 p-0 text-danger">You don't have right access to buy</p>}
                <div className="d-flex flex-column flex-md-row justify-content-md-between align-items-md-center">
                    <div className="btn-group">
                        <Button variant="outline-secondary"
                            className="btn btn-sm btn-outline-secondary"
                            onClick={() => showOfferCondition(offer)}
                        >Conditions</Button>
                        {addToCartButton()}
                        <Overlay target={target.current} show={show} placement="top">
                            {({ placement, arrowProps, show: _show, popper, ...props }) => (
                                <div
                                    {...props}
                                    style={{
                                        backgroundColor: 'rgba(0, 0, 0, 0.6)',
                                        padding: '2px 10px',
                                        color: 'white',
                                        borderRadius: 3,
                                        ...props.style,
                                    }}
                                >
                                    Item added to cart!
                                </div>
                            )}
                        </Overlay>
                    </div>
                    {!interactive && (
                        <span style={{ fontSize: '12px' }}>
                            <span className={`badge badge-pill badge-${getClass(offer.left || 0)}`}>
                                {offerExists ? (offer.left + ' Left') : 'Sold Out'}
                            </span>
                            {timeLeft && <span className="badge badge-pill badge-warning ml-2">
                                {`${timeLeft.days}Day(s) ${timeLeft.hours}hrs ${timeLeft.minutes}mins ${timeLeft.seconds} `}
                            </span>}
                        </span>

                    )}
                </div>
            </Fragment>

        )
    }

    function isInteractive() {
        return (
            <div className="single-product card mb-4 shadow-sm">
                <div id="msg" />
                <input ref={inputFileRef} onChange={handleChange} type="file" className="file-hidden" accept="image/*" />
                <div
                    className="img-holder interactive"
                    onClick={() => inputFileRef.current.click()}
                    style={{ backgroundImage: `url(${offer.images[0]})`, cursor: 'pointer' }} />
                <div className="card-body">
                    <input className="form-control mb-2" placeholder="Add product name" value={offer.productName} onChange={(e) => changeHappened({ productName: e.target.value })} />
                    <input className="form-control mb-2" placeholder="Add product description" value={offer.productDescription} onChange={(e) => changeHappened({ productDescription: e.target.value })} />
                    <textarea className="form-control mb-2" placeholder="Add product details" value={offer.details} data-row={10}
                        onChange={(e) => changeHappened({ details: e.target.value })} />
                    {cardFooter()}
                    <div className="app-badge-overlay">
                        <span className={`top-right app-badge ${badgeColor}`}>Save {getAmount(Number(offer.marketPrice) - Number(offer.offerPrice))}</span>
                    </div>
                    <div className="mt-3">
                        <InputGroup size="sm" className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="inputGroup-sizing-sm">Market Price</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm"
                                value={offer.marketPrice}
                                onChange={(e) => changeHappened({ marketPrice: e.target.value })} />
                        </InputGroup>

                        <InputGroup size="sm" className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="inputGroup-sizing-sm">Offer Price</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm"
                                value={offer.offerPrice}
                                onChange={(e) => changeHappened({ offerPrice: e.target.value })} />
                        </InputGroup>

                        <InputGroup size="sm" className="mb-3">
                            <InputGroup.Prepend>
                                <InputGroup.Text id="inputGroup-sizing-sm"> Pieces</InputGroup.Text>
                            </InputGroup.Prepend>
                            <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm"
                                value={offer.left}
                                onChange={(e) => changeHappened({ left: e.target.value })} />
                        </InputGroup>
                    </div>
                </div>
            </div>
        )
    }

    function nonInteractive() {
        return (
            <div className="single-product card mb-4 shadow-sm">
                <img className="bd-placeholder-img card-img-top" src={offer.images[0]} alt={offer.productDescription} width="100%" />
                <div className="card-body">
                    <p className="card-text">{offer.productName}</p>
                    <p className="card-text">{offer.productDescription}</p>
                    {showDetails && <p className="card-text">{offer.details}</p>}
                    {cardFooter()}
                    <div className="app-badge-overlay">
                        <span className={`top-right app-badge ${badgeColor}`}>Save {getAmount(Number(offer.marketPrice) - Number(offer.offerPrice))}</span>
                    </div>
                </div>
            </div>
        )
    }

    return (
        <Fragment>
            {interactive ? isInteractive() : nonInteractive()}
            <Modal show={showModal}>
                <Modal.Header>
                    <h5 className="modal-title">{selectedOffer.productDescription}</h5>
                </Modal.Header>
                <Modal.Body>
                    {interactive ? (
                        <textarea
                            className="form-control"
                            placeholder="Add Product or sales condition"
                            value={offer.conditions} rows={10}
                            onChange={(e) => changeHappened({ conditions: e.target.value })} />
                    ) : (
                            <p>{selectedOffer.conditions}</p>
                        )}
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        variant='secondary'
                        onClick={() => toggleModal(!showModal)}
                    >Close</Button>
                </Modal.Footer>
            </Modal>
        </Fragment>
    )
}