import './Header.scss';

import React, { ReactElement } from 'react';
import Button from 'react-bootstrap/esm/Button';

export function Header(): ReactElement {
    const image = 'https://www.sammobile.com/wp-content/uploads/2020/07/Samsung-Galaxy-M01s.jpg';
    return (
        <header className="container app-header-container">
            <div
                className="app-header"
                style={{ backgroundImage: `url(${image})` }}>
                <div className="intro w-auto mb-3">
                    <h2 className="m-0 p-0">Price Drop!</h2>
                    <p>Up to 30% On All Products</p>
                </div>
                <Button variant="warning">Shop Now</Button>
            </div>
        </header>

    )
}