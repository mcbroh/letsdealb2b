import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';

dayjs.extend(utc);
dayjs.extend(timezone);

export const URLAddress = process.env.NODE_ENV !== 'production' ? 'http://localhost:5001/agency-cheriff/us-central1/api/' : 'https://us-central1-agency-cheriff.cloudfunctions.net/api/';


export const _capitalize = (string: string): string => string.charAt(0).toUpperCase() + string.slice(1);

export const _calculateTimeLeft = (endDate: string | Date | undefined) => {

    if (endDate) {
        let difference = dayjs(endDate).tz('Africa/Lagos').diff(dayjs().tz('Africa/Lagos'));
        let timeLeft = null;

        if (difference > 0) {
            timeLeft = {
                days: Math.floor(difference / (1000 * 60 * 60 * 24)),
                hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
                minutes: Math.floor((difference / 1000 / 60) % 60),
                seconds: Math.floor((difference / 1000) % 60)
            };
        }

        return timeLeft;
    }
    return null;
};