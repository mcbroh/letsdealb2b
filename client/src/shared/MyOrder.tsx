import React, { useEffect, useState } from 'react';
import Table from 'react-bootstrap/Table';
import Image from 'react-bootstrap/esm/Image';

import './Cart.scss';
import { useAppDispatch, useAppState } from '../components/hooks';
import { useHistory } from 'react-router-dom';
import { URLAddress } from '../helper';

export function MyOrder() {
    const history = useHistory();
    const dispatch = useAppDispatch();
    const { user, offers } = useAppState();
    const [orders,  setOrders] = useState<Array<any>>([]);

    useEffect(() => {
        updateMyOrder();
        // eslint-disable-next-line
    }, [user?.products]);

    function updateMyOrder() {
        if (offers.length < 1) {
            fetch(URLAddress + 'products/list')
            .then(response => {
                response.json().then(data => {
                    dispatch({ type: 'setOffers', data: data.products });
                    sortOrders(data.products);
                });
            })
            .catch();
        } else sortOrders();
    }

    function sortOrders(allOffers = offers) {
        const orders: Array<any> = [];
        (user?.products || []).forEach((order: any) => {
            for (let index = 0; index < order.booked; index++) {
                orders.push(allOffers.find((offer: any) => offer.id === order.productID));
            }
        });
        return setOrders(orders);
    };

    useEffect(() => {
        if (!user || user.role !== 'subBuyer') {
            history.replace('/');
        }
    })

    const totalPrice = (offer: any) => offer.reduce((a: any, b: any) => ({ offerPrice: Number(a.offerPrice) + Number(b.offerPrice) }));
    const cartHasProduct = () => orders.length > 0;

    return cartHasProduct() ? (
        <Table striped hover className="app-cart">
            <tbody>
                {orders.map((picked: any, index) =>
                    <tr key={index}>
                        <td>
                            <Image
                                src={picked.images[0]}
                                alt={'text'} rounded />
                            <p className="d-md-none d-inline ml-2">{picked.productName}</p>
                        </td>
                        <td className="d-none d-md-block">
                            {picked.productName}
                        </td>
                        <td className="value-holder">₦ {picked.offerPrice}</td>
                        <td className="text-warning"><b>Undelivered</b></td>
                    </tr>
                )}
                <tr>
                    <td className="d-none d-md-block"></td>
                    <td className="text-right" colSpan={3}><b>Total:</b> ₦ {totalPrice(orders).offerPrice}</td>
                </tr>
            </tbody>
        </Table>
    ) : (
        <p className="text-center">{user?.products?.length > 0 ? 'Loading your orders...' : 'You have not created any order yet'}</p>
    )
}