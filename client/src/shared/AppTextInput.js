import React from 'react';
import { useField, Field } from 'formik';

export function AppTextInput({ label, clx = '', ...props }) {
    const [field, meta] = useField(props);

    const errorText = meta.error && meta.touched ? meta.error : '';
    
    return (
        <div className={`form-group ${clx}`}>
            <label htmlFor={props.name}>{label}</label>
            <Field className="form-control" {...field} {...props} />
            {errorText && <div className="invalid-feedback d-block">{errorText}</div>}
        </div>
    )
}