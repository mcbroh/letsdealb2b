import React from 'react';
import Table from 'react-bootstrap/Table';
import Image from 'react-bootstrap/esm/Image';
import { X as CloseIcon } from 'react-bootstrap-icons';

import './Cart.scss';
import { useAppDispatch } from '../components/hooks';

export function Cart({ cart, canDelete }: any) {
    const dispatch = useAppDispatch();

    const totalPrice = (offer: any) => offer.reduce((a: any, b: any) => ({ offerPrice: Number(a.offerPrice) + Number(b.offerPrice) }));
    const cartHasProduct = () => cart.length > 0;

    return cartHasProduct() ? (
        <Table striped hover className="app-cart">
            <tbody>
                {cart.map((picked: any) =>
                    <tr key={picked.tempKey}>
                        <td>
                            <Image
                                src={picked.images[0]}
                                alt={'text'} rounded />
                            <p className="d-md-none d-inline ml-2">{picked.productName}</p>
                        </td>
                        <td className="d-none d-md-block">
                            {picked.productName}
                        </td>
                        <td className="value-holder">₦ {picked.offerPrice}</td>
                        {canDelete && <td><CloseIcon onClick={() => dispatch({ type: 'removeFromCart', data: picked })} /></td>}
                    </tr>
                )}
                <tr>
                    <td className="d-none d-md-block"></td>
                    <td className="text-right" colSpan={3}><b>Total:</b> ₦ {totalPrice(cart).offerPrice}</td>
                </tr>
            </tbody>
        </Table>
    ) : (
        <p className="text-center">Cart is empty</p>
    )
}