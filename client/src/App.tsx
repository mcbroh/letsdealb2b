import './App.scss';

import React from 'react';
import { Route, BrowserRouter as Router } from 'react-router-dom';

import { NavBar } from './components/NavBar';
import { ProductList } from './components/Product/ProductList';
import { AppProvider } from './components/hooks';
import { CheckOut } from './components/Product/CheckOut';
import Admin from './components/Admin/Admin';
import Invoice from './components/Invoice/Invoice';
import Seller from './components/Seller/Seller';
import { AppAlert } from './components/AppAlert';
import { Register } from './components/Admin/Register';

const Main = () => (
    <Router>
        <NavBar />
        <Route exact path="/" component={ProductList} />
        <Route path="/checkout" component={CheckOut} />
        <Route exact path="/admin" component={Admin} />
        <Route exact path="/trade" component={Invoice} />
        <Route exact path="/seller" component={Seller} />
        <Route exact path="/register" component={Register} />
        <AppAlert />
    </Router>
)

function App() {
    return (
        <div className="App">
            <AppProvider>
                <Main />
            </AppProvider>
        </div>
    );
}

export default App;
