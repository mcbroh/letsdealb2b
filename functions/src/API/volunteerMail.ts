export const volunteerMail = ({ subject, message, title }: any): any => ({
    from: 'contact@renewingpurpose.org',
    to: 'contact@renewingpurpose.org',
    subject: subject,
    html: `<body style="margin: 0; padding: 0;"> 
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                    <tr>
                        <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                        <b>${title}</b>
                                    </td>
                                </tr>
                                ${message.map((m: any) => `
                                <tr>
                                    <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                        <b>${m.label}:</b> ${m.value}
                                    </td>
                                </tr>`
                                )}
                                <tr>
    
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>                
                </table>
            </td>
        </tr>
    </table>
</body>`
})