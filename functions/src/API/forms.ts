import { Request, Response } from "express";
import * as nodemailer from 'nodemailer';
import { thankYouMail } from "./thankyouMail";
import { volunteerMail } from "./volunteerMail";

const credentials = {
    host: 'smtp.zoho.com',
    Port: 465,
    secure: true,
    auth: {
        user: 'contact@renewingpurpose.org',
        pass: 'V43L0SRYgvtR'
    }
};

export const volunteer = async (request: Request, response: Response) => {
    const updatedData = JSON.parse(request.body);
    
    const transporter = nodemailer.createTransport(credentials);

    // send mail with defined transport object
    const info = await transporter.sendMail(thankYouMail({
        email: updatedData.email.value,
        name: updatedData.firstName.value + ' ' + updatedData.lastName.value, 
        subject: 'Application received', 
        message: 'Thank you for volunteering, we will mail you soon'
    }));

    console.log("Message sent: %s", info.messageId);

    const copy = await transporter.sendMail(volunteerMail({
        title: 'New Volunteer',
        subject: 'New Application', 
        message: Object.values(updatedData)
    }));
    console.log("Message sent: %s", copy.messageId);

    response.json({ updatedData });
};

export const referral = async (request: Request, response: Response) => {
    const updatedData = JSON.parse(request.body);

    const transporter = nodemailer.createTransport(credentials);

    // send mail with defined transport object
    const info = await transporter.sendMail(thankYouMail({
        email: updatedData.email.value,
        name: updatedData.fullName.value, 
        subject: 'Your referral has been received!', 
        message: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ornare, dolor sed laoreet ornare, nisl ligula elementum metus, at maximus urna nunc in metus. Mauris molestie lectus sem, commodo porttitor nibh rutrum id. Integer molestie luctus lacinia. Nullam at quam vitae neque vulputate feugiat sed at justo. Sed luctus urna a vestibulum tincidunt. Cras condimentum eget sapien sit amet fermentum. Aenean efficitur quis odio vitae aliquet. Curabitur nec erat leo.

        Pellentesque tincidunt iaculis pretium. Etiam feugiat dignissim ultrices. Quisque mollis sem a enim lobortis, nec viverra justo tristique. Donec eu lobortis odio, sed feugiat mauris. Duis hendrerit eleifend sollicitudin. Mauris sagittis varius scelerisque. Cras pretium eget dolor a aliquet. Pellentesque dignissim euismod nisl nec semper. Aenean lacinia, magna sed molestie malesuada, felis ex semper ante, ut consectetur metus leo ut nulla. Phasellus at volutpat est.`
    }));

    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

    const copy = await transporter.sendMail(volunteerMail({
        title: 'New Referral',
        subject: 'New Referral', 
        message: Object.values(updatedData)
    }));
    console.log("Message sent: %s", copy.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(copy));

    response.json({ updatedData });
};