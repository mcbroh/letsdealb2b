export interface NewBuyer {
    email: string;
    token?: string;
    fullName: string;
    password: string;
    telephone: string;
}

export interface BuyerCompanyData {
    role: 'buyer';
    companyName: string;
    companyEmail: string;
    companyTelephone: string;
    companyID: string;
}
