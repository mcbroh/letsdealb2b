import * as firebase from 'firebase';

import { db } from '../../util/admin';
import { getErrorMessages } from '../../util/helpers';
import { buyerSchema } from './schema';
import { NewBuyer, BuyerCompanyData } from './buyerInterface';

const usersRef = db.collection('users');
const registerTokenRef = db.collection('registerToken');

export const signUpBuyer = async (request: any, response: any) => {
    const { body } = request;

    const newUser: NewBuyer = {
        token: body.token,
        fullName: body.fullName,
        email: body.email,
        password: body.password,
        telephone: body.telephone,
    };

    const { error } = buyerSchema.validate(newUser);
    if (error) {
        return response.status(400).json({ message: getErrorMessages(error) });
    }

    try {
        const tokenDetails = (await registerTokenRef.doc(body.token).get()).data();
        if (!tokenDetails || tokenDetails.role !== 'subBuyer') return response.status(500).json({ message: 'Invalid token' });
        const companyID = tokenDetails?.creator;
        const companyDetails = (await usersRef.doc(companyID).get()).data();
        if (!companyDetails) return response.status(500).json({ message: 'Company not found, Discuss with you company administrator' });

        const userCredential = await firebase
            .auth()
            .createUserWithEmailAndPassword(
                newUser.email,
                newUser.password
            );

        const userId = userCredential?.user?.uid as any;

        if (userId) {
            const buyerObject: NewBuyer & BuyerCompanyData = {
                role: tokenDetails.role,
                companyName: companyDetails.companyName,
                companyEmail: companyDetails.companyEmail,
                companyTelephone: companyDetails.companyTelephone,
                companyID,
                ...newUser,
            }

            await usersRef.doc(userId).set(buyerObject);
            await usersRef.doc(companyID).update({ workers: [...(companyDetails.workers || []), userId] });
        }

        return response.status(200).json({ success: true });

    } catch (error) {
        if (error.code === 'auth/email-already-in-use') {
            return response.status(400).json({ message: 'Email already in use' });
        } else {
            return response.status(500).json({ message: 'Something went wrong, please try again', error });
        }
    }
}

export const myMembers = async (request: any, response: any) => {
    const { user } = request;

    try {
        if (user.role !== 'buyer') return response.status(400).json({ message: 'You do not have the correct access!' });
        const docs = await usersRef.where('companyEmail', '==', user.companyEmail).get();

        const dataToClient: Array<any> = [];
        docs.forEach(doc => {
            const role = doc.data().role;
            role === 'subBuyer' && dataToClient.push({ id: doc.id, ...doc.data() });
        });
        return response.status(200).json({ success: true, members: dataToClient });
    } catch (error) {
        return response.status(500).json({ message: 'Something went wrong, please try again', error });
    }
}