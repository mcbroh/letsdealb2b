import * as Joi from "joi";

export const buyerSchema = Joi.object().keys({
    fullName: Joi.string().min(7).max(50).required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(6).max(50).required(),
    telephone: Joi.string().required(), // https://www.npmjs.com/package/joi-phone-number
    token: Joi.string().alphanum().required(),
});
