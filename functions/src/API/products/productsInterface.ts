export interface ProductAmountLeft {
    id?: string;
    left: number;
    booked: number;
}

export interface ProductContent {
    conditions: string;
    details: string;
    marketPrice: string;
    offerPrice: string;
    productName: string;
    booked?: number;
    productDescription: string;
    qualifyingPieces: number;
    images: Array<string>;
    hidden?:boolean;
}