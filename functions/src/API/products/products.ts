import * as cloudinary from 'cloudinary';
import * as dayjs from 'dayjs';
import * as utc from 'dayjs/plugin/utc';
import * as timezone from 'dayjs/plugin/timezone';

import { db } from '../../util/admin';
import { getErrorMessages } from '../../util/helpers';
import { productSchema } from './schema';
import { ProductContent, ProductAmountLeft } from './productsInterface';
import { cloudinaryConfig } from '../../util/config';

dayjs.extend(utc);
dayjs.extend(timezone);

const productsRef = db.collection('products');
const usersRef = db.collection('users');

export const listProducts = async (request: any, response: any) => {
    try {
        const rawProducts = await productsRef.get();
        const products: Array<ProductContent & { id: string }> = [];
        rawProducts.forEach(product => {
            if (product.data().endDate) {
                const today = dayjs(new Date()).tz('Africa/Lagos');
                const endDate = dayjs(product.data().endDate).tz('Africa/Lagos');

                const diff = endDate.diff(today);
                products.push({ id: product.id, ...product.data() as ProductContent, hidden: diff < 10 })
            } else {
                products.push({ id: product.id, ...product.data() as ProductContent })
            }
        });
        return response.status(200).json({ success: true, products });

    } catch (error) {
        return response.status(500).json({ message: 'Something went wrong, please try again' });
    }
}

export const refreshListProducts = async (request: any, response: any) => {
    try {
        const rawProducts = await productsRef.get();
        const products = getProductAmountsLeft(rawProducts);
        return response.status(200).json({ success: true, products });

    } catch (error) {
        return response.status(500).json({ message: 'Something went wrong, please try again' });
    }
}

export const myProducts = async (request: any, response: any) => {
    const { user } = request;

    try {
        if (user.role !== 'seller') return response.status(400).json({ message: 'You do not have the correct access!' });
        const docs = await productsRef.where('owner', '==', user.uid).get();

        const dataToClient: Array<any> = [];
        docs.forEach(doc => {
            dataToClient.push({ id: doc.id, ...doc.data() });
        });
        return response.status(200).json({ success: true, products: dataToClient });
    } catch (error) {
        return response.status(500).json({ message: 'Something went wrong, please try again', error });
    }
}


export const submitProduct = async (request: any, response: any) => {
    const { user, body } = request;

    const { error } = productSchema.validate(body);
    if (error) return response.status(400).json({ message: getErrorMessages(error) });
    if (user.role !== 'seller') return response.status(400).json({ message: 'Unauthorized to perform action' });

    try {
        const offer = { ...body };
        offer.images = [];
        offer.imagePublicId = [];
        offer.startDate = dayjs(body.startDate).tz('Africa/Nigeria');
        const created = Date.now().toString();
        const imageBank = cloudinary.v2;
        imageBank.config(cloudinaryConfig);
        await Promise.all(request.body.images.map(async (image: any, index: number) => {
            await imageBank.uploader.upload(
                image,
                { public_id: `offers/${created + index}` },
                (err, data) => {
                    if (err) return response.status(200).json({ success: false, message: 'There was an issue with the image', err });
                    offer.images.push(data?.secure_url);
                    offer.imagePublicId.push(`offers/${created + index}`);
                }
            );
        }));

        offer.qualifyingPieces = Number(offer.left);
        await productsRef.add({ ...offer, owner: user.uid });

        const docs = await productsRef.where('owner', '==', user.uid).get();

        const dataToClient: Array<any> = [];
        docs.forEach(doc => {
            dataToClient.push({ id: doc.id, ...doc.data() });
        });

        return response.status(200).json({ success: true, products: dataToClient });

    } catch (error) {
        return response.status(500).json({ message: 'Something went wrong, please try again', error });
    }
}

export const buyProducts = async (request: any, response: any) => {
    const { user, body } = request;

    if (user.role !== 'subBuyer') return response.status(500).json({ message: 'You cannot buy with this role' });

    try {
        const rawProducts = await productsRef.get();
        const products = getProductAmountsLeft(rawProducts, true) as { [state: string]: ProductAmountLeft };

        const productsToBuy: { [state: string]: { toBook: number } & ProductAmountLeft } = {};
        const potentialProducts: Array<{ id: string; quantity: number }> = body.products;
        const productsNotBooked: { [state: string]: { booked: number; left: number } } = {}

        potentialProducts.forEach(prod => {
            if ((prod.quantity <= products[prod.id].left) && (products[prod.id].left !== 0)) {
                productsToBuy[prod.id] = {
                    toBook: prod.quantity,
                    ...products[prod.id]
                }
            } else {
                productsNotBooked[prod.id] = {
                    booked: prod.quantity,
                    left: products[prod.id].left
                };
            }
        });

        // @Todo update seller most wanted
        const batch = db.batch();
        Object.keys(productsToBuy).forEach(key => {
            const currentProduct = productsToBuy[key];
            batch.update(productsRef.doc(key), {
                booked: currentProduct.booked + currentProduct.toBook,
                left: currentProduct.left - currentProduct.toBook,
            });
        });
        await batch.commit();

        await usersRef.doc(user.uid).update({
            products: [
                ...(user.products || []),
                ...Object.keys(productsToBuy).map(key => ({
                    productID: productsToBuy[key].id,
                    booked: productsToBuy[key].toBook
                })),
            ]
        });

        const doc = await usersRef.doc(user.uid).get();

        return response.status(200).json({ success: true, user: doc.data(), message: productsNotBooked });

    } catch (error) {
        return response.status(500).json({ message: 'Something went wrong, please try again', error });
    }
}

function getProductAmountsLeft(rawProducts: any, asObject?: boolean): Array<ProductAmountLeft> | { [state: string]: ProductAmountLeft } {
    const products: Array<ProductAmountLeft> = [];
    const productsAsObject: { [state: string]: ProductAmountLeft } = {};
    rawProducts.forEach((product: any) => {
        const productDetails = product.data() as ProductContent;
        const prod = {
            id: product.id,
            left: productDetails.qualifyingPieces - (productDetails.booked || 0),
            booked: productDetails.booked || 0,
        };
        products.push(prod);
        asObject && (productsAsObject[product.id] = prod);
    });
    return (asObject ? productsAsObject : products);
}
