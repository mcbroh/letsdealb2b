import * as Joi from "joi";

export const productSchema = Joi.object().keys({
    conditions: Joi.string().required(),
    details: Joi.string().required(),
    marketPrice: Joi.string().required(),
    offerPrice: Joi.string().required(),
    productName:  Joi.string().min(5).max(50).required(),
    productDescription: Joi.string().min(10).max(100).required(),
    qualifyingPieces: Joi.number().required(),
    images: Joi.array().items(Joi.string()).required(),
    startDate: Joi.string().required(),
    endDate: Joi.string().required(),
    left: Joi.string().required(),
});
