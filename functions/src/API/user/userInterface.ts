export type Role = 'admin' | 'seller' | 'buyer' | 'invoice' | 'subBuyer';

export interface NewUser {
    role?: Role;
    token: string;
    email: string;
    fullName: string;
    password: string;
    telephone: string;
    companyName: string;
    companyEmail: string;
    companyTelephone: string;
    memberId?:string;
    rights?: Array<string>;
}