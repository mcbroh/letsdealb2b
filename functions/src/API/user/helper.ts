export const roles = {
    admin: {
        roleName: 'Admin',
        canShow: 'superAdmin',
        rights: [
            'superAdmin',
            'admin',
            'canViewSellers',
            'canGenerateSellers',
            'canViewBuyers',
            'canGenerateBuyers',
            'canViewInvoices',
            'canGenerateInvoices',
            'canDisable',
            'canEnable'
        ]
    },
    seller: {
        roleName: 'Seller',
        override: 'superAdmin',
        canShow: 'canGenerateSellers',
    },
    buyer: {
        roleName: 'Buyer',
        override: 'superAdmin',
        canShow: 'canGenerateBuyers',
    },
    invoice: {
        roleName: 'Invoice',
        override: 'superAdmin',
        canShow: 'canGenerateInvoices',
    },
};