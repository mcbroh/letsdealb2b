import * as firebase from 'firebase';
import * as jwt from 'jsonwebtoken';
import * as nodemailer from 'nodemailer';

import { db } from '../../util/admin';
import { validateLoginData } from "../../util/validators";
import { NewUser, Role } from './userInterface';
import { getRandomCharacters, getErrorMessages } from '../../util/helpers';
import { registerUserSchema } from './schema';
import { Token } from '../../util/config';
import { roles } from './helper';

const usersRef = db.collection('users');
const registerTokenRef = db.collection('registerToken');

export const loginUser = async (request: any, response: any) => {

    const user = {
        email: request.body.email,
        password: request.body.password
    }

    const { valid, errors } = validateLoginData(user);
    if (!valid) return response.status(400).json({ message: 'Please fill all required fields', errors });

    try {
        const signedInUser = await firebase
            .auth()
            .signInWithEmailAndPassword(
                user.email,
                user.password
            );

        const userId = signedInUser?.user?.uid;
        const token = jwt.sign(
            { uid: userId },
            Token
        );
        let userDetails = {} as any;

        if (userId) {
            await usersRef.doc(userId).update({ token: token });
            const doc = await usersRef.doc(userId).get();
            userDetails = doc.data();
            if (userDetails.role === 'admin') {
                userDetails.allBuyer = [];
                userDetails.allSeller = [];
                userDetails.allInvoice = [];
                const allUsers = await usersRef.get();
                userDetails.allActivities = [];
                let rawRegRefs;
                const isSuperAdmin = userDetails.rights.includes('superAdmin');
                if (isSuperAdmin) {
                    rawRegRefs = await registerTokenRef.get();
                    userDetails.otherUsers = await getUserByRole(allUsers, 'companyId', userDetails.companyId);
                } else {
                    rawRegRefs = await registerTokenRef.where('creator', '==', userId).get();
                    userDetails.otherUsers = await getUserByRole(allUsers, 'email', userDetails.email);
                }

                rawRegRefs.forEach(regRef => userDetails.allActivities.push({ id: regRef.id, ...regRef.data() }));
                (isSuperAdmin || userDetails.rights.includes('canViewSellers')) && (userDetails.allSeller = await getUserByRole(allUsers, 'role', 'seller'));
                (isSuperAdmin || userDetails.rights.includes('canViewBuyers')) && (userDetails.allBuyer = await getUserByRole(allUsers, 'role', 'buyer'));
                (isSuperAdmin || userDetails.rights.includes('canViewInvoices')) && (userDetails.allInvoice = await getUserByRole(allUsers, 'role', 'invoice'));
                userDetails.roles = roles;
            }

        } else {
            return response.status(401).json({ message: 'wrong credentials, please try again' });
        }

        return response.status(200).json({ success: true, user: { ...userDetails, token } });

    } catch (error) {
        return response.status(401).json({ message: 'wrong credentials, please try again', error });
    }
};

async function getUserByRole(allUsers: any, role: string, arg: string): Promise<Array<any>> {
    const usersByRole: Array<any> = [];
    allUsers.forEach((user: any) => {
        
        if (user.data()[role] === arg) {
            const fullData: any = {
                companyEmail: user.data().companyEmail,
                companyName: user.data().companyName,
                companyId: user.data().companyId,
                companyTelephone: user.data().companyTelephone,
                email: user.data().email,
                fullName: user.data().fullName,
                role: user.data().role,
                rights: user.data().rights,
            }
            usersByRole.push({ id: user.id, ...fullData })
        }
    });

    return usersByRole;
}

export const signUpUser = async (request: any, response: any) => {
    const { body } = request;

    const newUser: NewUser = {
        token: body.token,
        fullName: body.fullName,
        email: body.email,
        password: body.password,
        telephone: body.telephone,
        companyName: body.companyName,
        companyEmail: body.companyEmail,
        companyTelephone: body.companyTelephone,
    };

    const { error } = registerUserSchema.validate(newUser);
    if (error) {
        return response.status(400).json({ message: getErrorMessages(error) });
    }

    try {
        const tokenDetails = (await registerTokenRef.doc(newUser.token).get()).data();
        if (!tokenDetails || tokenDetails.role === 'subBuyer' || tokenDetails.used) return response.status(500).json({ message: 'Invalid token' });

        const userCredential = await firebase
            .auth()
            .createUserWithEmailAndPassword(
                newUser.email,
                newUser.password
            );

        const userId = userCredential?.user?.uid as any;

        if (userId) {
            newUser.role = tokenDetails.role;
            newUser.rights = tokenDetails.rights;
            if (newUser.role === 'buyer') {
                const memberToken = getRandomCharacters();
                newUser.memberId = memberToken;
                await registerTokenRef.doc(memberToken).set({
                    role: 'subBuyer',
                    creator: userId,
                });
            }

            await usersRef.doc(userId).set(newUser);
            await registerTokenRef.doc(newUser.token).update({ used: true, usedBy: userId, usedDate: Date.now() });
        }

        return response.status(200).json({ success: true });

    } catch (error) {
        if (error.code === 'auth/email-already-in-use') {
            return response.status(400).json({ message: 'Email already in use' });
        } else {
            return response.status(500).json({ message: 'Something went wrong, please try again' });
        }
    }
}

export const addMember = async (request: any, response: any) => {
    const { body, user } = request;

    const newUser = {
        fullName: body.fullName,
        email: body.email,
        role: body.role,
        telephone: body.contactTelephone,
    };


    if (!(newUser.fullName || newUser.email || newUser.telephone || newUser.role)) {
        return response.status(400).json({ message: 'Please fill all required fields' });
    }

    try {
        await usersRef.doc(`${user.uid}/members/`).set({
            role: newUser.role,
            fullName: newUser.fullName,
            telephone: newUser.telephone,
            email: newUser.email,
            memberCode: Date.now().toString(),
        });

        return response.status(200).json({ success: true });

    } catch (error) {
        return response.status(500).json({ message: 'Something went wrong, please try again' });
    }
}

export const generateCode = async (request: any, response: any) => {
    const { body, user } = request;
    const { role, email, rights = [] } = body;

    if (!role || !email) return response.status(400).json({ message: 'Email or role is missing' });

    const allowedRoles = ['admin', 'seller', 'invoice'] as Array<Role>;
    if (!allowedRoles.includes(role)) return response.status(400).json({ message: 'Role not allowed' });
    if (role === 'admin' && rights.length < 1) return response.status(400).json({ message: 'Rights must be specified' });

    try {
        const token = getRandomCharacters();
        await registerTokenRef.doc(token).set({
            role,
            creator: user.uid,
            email,
            rights,
            created: Date.now(),
        });
        const mailInfo = await mailRegistrationToken({ token, email })
        return response.status(200).json({ success: true, message: nodemailer.getTestMessageUrl(mailInfo) });

    } catch (error) {
        return response.status(500).json({ message: 'Something went wrong, please try again' });
    }
}

export const updateUser = async (request: any, response: any) => {
    const {  body } = request;
    console.log(body.id);
    
    try {
        await usersRef.doc(body.id).set({
            active: Boolean(body.active)
        });
        return response.status(200).json({ success: true });

    } catch (error) {
        return response.status(500).json({ message: 'Something went wrong, please try again' });
    }
};

async function mailRegistrationToken({ token, email }: { token: string; email: string }) {
    // Generate test SMTP service account from ethereal.email
    // Only needed if you don't have a real mail account for testing
    const testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: testAccount.user, // generated ethereal user
            pass: testAccount.pass, // generated ethereal password
        },
    });

    // send mail with defined transport object
    const info = await transporter.sendMail({
        from: '"Lets Deal B2B" <info@letsdealb2b.com>',
        to: email,
        subject: "Registration token",
        text: `Your registration token is: ${token}`,
        html: `Your registration token is: <b>${token}</b>`,
    });

    console.log("Message sent: %s", info.messageId);
    return info;
}