import * as jwt from 'jsonwebtoken';
import { db } from './admin';
import { Token } from './config';

const usersRef = db.collection('users');
export const auth = async (request: any, response: any, next: any) => {
    let token;
    if (request.headers.authorization && request.headers.authorization.startsWith('Bearer ')) {
        token = request.headers.authorization.split('Bearer ')[1];
    } else {
        return response.status(401).json({ message: 'Unauthorized, You need to be signed in to perform action' });
    }

    try {
        const decodedToken = jwt.verify(token, Token) as any;
        const user = (await usersRef.doc(decodedToken.uid).get()).data();
        if (user?.token !== token) {
            return response.status(401).json({ message: 'Unauthorized, You need to be signed in to perform action' });
        }
        request.user = {
            uid: decodedToken.uid,
            ...user,
        }
       return next();
    } catch (error) {
        return response.status(401).json({ message: error });
    }
};

export const admin = async (request: any, response: any, next: any) => {
    let token;
    if (request.headers.authorization && request.headers.authorization.startsWith('Bearer ')) {
        token = request.headers.authorization.split('Bearer ')[1];
    } else {
        return response.status(401).json({ message: 'Unauthorized, You need to be signed in to perform action' });
    }

    try {
        const decodedToken = jwt.verify(token, Token) as any;
        const user = (await usersRef.doc(decodedToken.uid).get()).data();        
        if (user?.token !== token || user?.role !== 'admin') {
            return response.status(401).json({ message: 'Unauthorized, You need to be signed as Admin in to perform action' });
        }
        request.user = {
            uid: decodedToken.uid,
            ...user,
        }
       return next();
    } catch (error) {
        return response.status(401).json({ message: error });
    }
};