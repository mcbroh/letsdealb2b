export function getRandomCharacters() {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    const charactersLength = characters.length;
    for (let i = 0; i < 2; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return Date.now().toString() + result;
}

export function getErrorMessages(errorObject: any) {
    const messages = errorObject.details.map((error: {message: string})=> error.message).join(',');
    return messages;
}