import { NewUser } from "../API/user/userInterface";

const isEmpty = (value: string) => {
    if (!value) return true;
	if (value.trim() === '') return true;
	else return false;
};

export const validateLoginData = (data: {email: string; password: string}) => {
   const errors = {} as any;
   if (isEmpty(data.email)) errors.email = 'Must not be empty';
   if (isEmpty(data.password)) errors.password = 'Must not be  empty';
   return {
       errors,
       valid: Object.keys(errors).length === 0 ? true : false
    };
};

const isEmail = (email: string) => {
	const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (email.match(emailRegEx)) return true;
	else return false;
};

export const validateSignUpData = (data: NewUser) => {
	const errors = {} as any;

	if (isEmpty(data.email)) {
		errors.email = 'Must not be empty';
	} else if (!isEmail(data.email)) {
		errors.email = 'Must be valid email address';
	}

	if (isEmpty(data.fullName)) errors.fullName = 'Must not be empty';
    if (isEmpty(data.password)) errors.password = 'Must not be empty';
    if (isEmpty(data.telephone)) errors.telephone = 'Must not be empty';
	if (isEmpty(data.companyName)) errors.companyName = 'Must not be empty';
    if (isEmpty(data.companyEmail)) errors.companyEmail = 'Must not be empty';
	if (isEmpty(data.companyTelephone)) errors.companyTelephone = 'Must not be empty';

	return {
		errors,
		valid: Object.keys(errors).length === 0 ? true : false
	};
};