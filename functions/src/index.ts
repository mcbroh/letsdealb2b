import * as functions from 'firebase-functions';
import * as firebase from 'firebase';
import * as express from 'express';
import * as cors from 'cors';
import { firebaseConfig } from './util/config';


import { admin, auth } from './util/auth';
import { loginUser, signUpUser, addMember, generateCode, updateUser } from './API/user/user';
import { signUpBuyer, myMembers } from './API/buyers/buyers';
import { submitProduct, listProducts, refreshListProducts, buyProducts, myProducts } from './API/products/products';

firebase.initializeApp(firebaseConfig)

const app = express();
app.use(cors());

app.post('/login', loginUser);
app.post('/new-user', signUpUser);
app.post('/addMember', auth, addMember);

// admin
app.post('/update-user', admin, updateUser);
app.post('/generateCode', admin, generateCode);

// buyers
app.post('/buyer/register', signUpBuyer);
app.get('/buyer/members', auth, myMembers);

// Products
app.get('/products/list', listProducts);
app.get('/products/refresh-list', refreshListProducts);
app.post('/products/new',  auth, submitProduct);
app.post('/products/buy',  auth, buyProducts);
app.get('/products/my-products',  auth, myProducts)

exports.api = functions.https.onRequest(app);